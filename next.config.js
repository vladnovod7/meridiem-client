const withSass = require('@zeit/next-sass');
const withLess = require('@zeit/next-less');
const withCSS = require('@zeit/next-css');
const withImages = require('next-optimized-images');
const withPlugins = require('next-compose-plugins');

// fix: prevents error when .less files are required by node

if (typeof require !== 'undefined') {
  require.extensions['.less'] = () => {};
}

module.exports = withPlugins(
  [
    [
      withCSS,
      {
        cssModules: true,
        cssLoaderOptions: {
          importLoaders: 1,
          localIdentName: '[local]___[hash:base64:5]',
        },
        ...withLess(
          withSass({
            lessLoaderOptions: {
              lessOptions: {
                javascriptEnabled: true,
              },
            },
          }),
        ),
      },
    ],
    withImages,
  ],
  {
    typescript: {
      // !! WARN !!
      // Dangerously allow production builds to successfully complete even if
      // your project has type errors.
      // !! WARN !!
      ignoreBuildErrors: true,
    },
    publicRuntimeConfig: {
      API_PREFIX: process.env.NEXT_PUBLIC_API_PREFIX,
    },
  },
);
