module.exports = function (api) {
  api.cache(true);

  return {
    presets: ['@babel/preset-env', '@babel/preset-react', ['@babel/preset-typescript', { allowNamespaces: true }]],
    plugins: [
      '@babel/plugin-proposal-class-properties',
      '@babel/plugin-transform-runtime',
      '@babel/plugin-syntax-dynamic-import',
      [
        'styled-components',
        {
          ssr: true,
          displayName: true,
          preprocess: false,
        },
      ],
      [
        'import',
        {
          libraryName: 'antd',
          libraryDirectory: 'es',
          libraryDirectory: 'lib',
          style: 'index.css',
        },
      ],
      [
        'import',
        {
          libraryName: '@ant-design/icons',
          libraryDirectory: 'es/icons',
          libraryDirectory: 'lib/icons',
          camel2DashComponentName: false,
        },
        '@ant-design/icons',
      ],
    ],
  };
};
