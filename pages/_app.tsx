import React from 'react';
import { Header } from '@containers';
import { createGlobalStyle } from 'styled-components';
import { QueryClient, QueryClientProvider } from 'react-query';
import { createMockServer } from '@api';
import { useAuthContext, AuthContext } from '@lib/firebase';
import 'antd/dist/antd.css';
import '../styles/antd.less';

import '@assets/styles/reset.less';

const GlobalStyles = createGlobalStyle`
  :root {
    --primary: #7fb7be;
    --secondary: #D3F3EE;
    --primaryYellow: #DF9B6D;
    --lightGray: #fafafa;
    --error-color: #f5222d;
  }

  * {
    user-select: none;
  }

  #__next {
    min-height: 100%;
  }
`;

createMockServer({ environment: 'test' });

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  const data = useAuthContext();

  return (
    <AuthContext.Provider value={data}>
      <QueryClientProvider client={queryClient}>
        <GlobalStyles />
        <Header />
        <Component {...pageProps}></Component>
      </QueryClientProvider>
    </AuthContext.Provider>
  );
}

export default MyApp;
