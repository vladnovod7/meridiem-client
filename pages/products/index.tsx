import React from 'react';
import { createMockServer } from '@api';
import { Products } from '../../src/containers';

const IndexPage = () => {
  return <Products />;
};

export default IndexPage;
