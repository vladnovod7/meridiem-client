import styled from 'styled-components';

export const ItemTitle = styled.span`
  font-weight: 700;
`;

export const ItemValue = styled.p``;

export const ItemWrapper = styled.div`
  background: var(--lightGray);
  border-radius: 6px;
  padding: 10px;
`;

export const Wrapper = styled.div`
  width: 100%;
`;
