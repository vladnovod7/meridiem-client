import React from 'react';
import { Row, Col } from 'antd';
import * as Styled from './styles';

const PropertiesBlock = ({ properties }) => {
  return (
    <Styled.Wrapper>
      <Row gutter={[6, 6]}>
        {properties.map(({ id, title, types }) => (
          <Col key={id} span={8}>
            <Styled.ItemWrapper>
              <Styled.ItemTitle>{title}</Styled.ItemTitle>
              <Styled.ItemValue>{types[0].title}</Styled.ItemValue>
            </Styled.ItemWrapper>
          </Col>
        ))}
      </Row>
    </Styled.Wrapper>
  );
};

export default PropertiesBlock;
