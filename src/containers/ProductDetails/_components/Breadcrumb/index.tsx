import React from 'react';
import { Breadcrumb } from 'antd';
import * as Styled from './styles';

const ProductDetailsBreadcrumb = () => {
  return (
    <Styled.Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item>Домівка</Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="">Проектори</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="">Aсер</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>An Application</Breadcrumb.Item>
      </Breadcrumb>
    </Styled.Wrapper>
  );
};

export default ProductDetailsBreadcrumb;
