import React from 'react';
import { Button } from 'antd';
import * as Styled from './styles';

const PriceBlock = ({ price }) => {
  return (
    <Styled.Wrapper>
      <Styled.Price>{price}</Styled.Price>
      <Button size="large" type="primary">
        Купити
      </Button>
    </Styled.Wrapper>
  );
};

export default PriceBlock;
