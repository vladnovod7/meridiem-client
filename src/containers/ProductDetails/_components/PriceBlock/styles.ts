import styled from 'styled-components';
import { Typography } from 'antd';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Price = styled(Typography.Title)`
  align-self: flex-start;
  font-size: 30px;
  font-weight: 500;

  &::after {
    font-size: 18px;
    margin-left: 4px;
    content: 'грн';
  }
`;
