import React from 'react';
import { Image } from './styles';

const ImageBlock = ({ src }) => {
  return <Image src={src} />;
};

export default ImageBlock;
