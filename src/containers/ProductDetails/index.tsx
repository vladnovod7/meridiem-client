import React from 'react';
import { useRouter } from 'next/router';
import { Row, Col } from 'antd';
import { LayoutContainer } from '@ui-kit';
import { Breadcrumb, ImageBlock, PropertiesBlock, PriceBlock } from './_components';
import * as Styled from './styles';

const ProductDetails = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <Styled.Wrapper>
      <LayoutContainer>
        <Breadcrumb />
        <Styled.Title>{productData.name}</Styled.Title>
        <Styled.CardWrapper>
          <Row gutter={[40, 5]}>
            <Col span={12}>
              <ImageBlock src={productData.image} />
            </Col>
            <Col span={12}>
              <PropertiesBlock properties={productData.properties} />
              <PriceBlock price={productData.price} />
            </Col>
          </Row>
        </Styled.CardWrapper>
      </LayoutContainer>
    </Styled.Wrapper>
  );
};

export default ProductDetails;

const productData = {
  id: 4,
  image: 'https://www.dropbox.com/s/b0gc1iox1m1w23p/142116212.jpg?raw=1',
  name: 'AOpen AH15 (MR.JT611.001)',
  price: '11476',
  properties: [
    {
      id: 'matrix',
      title: 'Матриця',
      types: [
        {
          id: '2',
          title: 'DLP',
        },
      ],
    },
    {
      id: 'lamp',
      title: 'Лампа',
      types: [
        {
          id: '1',
          title: 'LED',
        },
      ],
    },
    {
      id: 'brightness',
      title: 'Яскравість',
      types: [
        {
          id: '3',
          title: '400-500',
        },
      ],
    },
    {
      id: 'diagonal',
      title: 'Діагональ',
      types: [
        {
          id: '2',
          title: '100-150',
        },
      ],
    },
    {
      id: 'dilatation',
      title: 'Розширення',
      types: [
        {
          id: '1',
          title: 'HD (1280/720)',
        },
      ],
    },
  ],
};
