import styled from 'styled-components';
import { Typography } from 'antd';
import { Wrapper as PropertiesWrapper } from './_components/PropertiesBlock/styles';

export const Wrapper = styled.div`
  background: var(--lightGray);
  min-height: 100%;
`;

export const Title = styled(Typography.Title)`
  margin-bottom: 40px;
`;

export const CardWrapper = styled.div`
  box-shadow: 0 1px 2px rgb(0 0 0 / 25%);
  background: white;
  padding: 15px;
  border-radius: 10px;

  ${PropertiesWrapper} {
    margin-bottom: 20px;
  }
`;
