import React, { FC } from 'react';
import { Row, Col, Typography } from 'antd';
import { StepForms, CheckoutSummary } from './_components';
import { LayoutContainer } from '@ui-kit';
import * as Styled from './styles';

const Checkout: FC = () => {
  return (
    <LayoutContainer>
      <Styled.Wrapper>
        <Row gutter={[16, 5]}>
          <Col span={19} offset={5}>
            <Typography.Title> Оформлення </Typography.Title>
          </Col>
          <Col span={10} offset={5}>
            <StepForms />
          </Col>
          <Col span={9}>
            <CheckoutSummary />
          </Col>
        </Row>
      </Styled.Wrapper>
    </LayoutContainer>
  );
};

export default Checkout;
