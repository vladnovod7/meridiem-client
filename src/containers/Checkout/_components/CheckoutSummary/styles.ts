import styled from 'styled-components';
import { Typography } from 'antd';
import { Wrapper as ProductItemWrapper } from './_components/ProductItem/styles';

export const Price = styled(Typography.Title)`
  margin-top: 15px;
`;

export const Wrapper = styled.div`
  padding: 15px;
  border-radius: 6px;
  border: 1px solid lightgray;

  ${ProductItemWrapper}:not(:last-child) {
    border-bottom: 1px solid lightgray;
  }
`;
