import React, { FC } from 'react';
import { ICartProduct } from '@interfaces';
import * as Styled from './styles';

const ProductsItem: FC<ICartProduct> = ({ image, name, count }: ICartProduct) => {
  return (
    <Styled.Wrapper>
      <Styled.Image src={image} />
      <Styled.DescriptionWrapper>
        <Styled.Title>{name}</Styled.Title>
        {count > 1 && <Styled.Counter>x {count}</Styled.Counter>}
      </Styled.DescriptionWrapper>
    </Styled.Wrapper>
  );
};

export default ProductsItem;
