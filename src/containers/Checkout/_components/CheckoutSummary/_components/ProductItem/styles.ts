import styled from 'styled-components';
import { Typography } from 'antd';

export const Image = styled.img`
  height: 80px;
  width: 80px;
  object-fit: contain;
  object-position: center;
  margin-right: 10px;
`;

export const Title = styled(Typography.Text)`
  margin-top: 15px;
`;

export const Counter = styled.span``;

export const DescriptionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Wrapper = styled.div`
  padding: 5px 0;
  display: flex;
  align-items: flex-start;

  ${Image} {
    flex-shrink: 1;
  }
`;
