import React, { FC } from 'react';
import { useCart } from '@hooks';
import { Typography } from 'antd';
import { ProductItem } from './_components';
import * as Styled from './styles';

const CheckoutSummary: FC<Record<string, never>> = () => {
  const { data } = useCart();

  const totalPrice = data?.reduce((acc, { price }) => acc + parseInt(price, 10), 0);

  return (
    <Styled.Wrapper>
      <Typography.Title level={4}>Склад замовлення</Typography.Title>
      {data?.map((productData) => (
        <ProductItem key={productData.id} {...productData} />
      ))}
      <Styled.Price level={4}>Сума: {totalPrice} грн</Styled.Price>
    </Styled.Wrapper>
  );
};

export default CheckoutSummary;
