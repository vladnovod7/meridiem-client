import { UserDataForm, DeliveryForm, PaymentForm } from './_components';
export const FORMS_KEYS = {
  personalData: 'personalData',
  delivery: 'delivery',
  payment: 'payment',
};

export const FORM_NAMES = {
  name: 'name',
  surname: 'surname',
  email: 'email',
  phone: 'phone',
  deliveryCity: 'deliveryCity',
  deliveryDepartment: 'deliveryDepartment',
  paymentMethod: 'paymentMethod',
};

import React from 'react';
export const initialFormsState = {
  [FORMS_KEYS.personalData]: {
    [FORM_NAMES.name]: '',
    [FORM_NAMES.surname]: '',
    [FORM_NAMES.email]: '',
    [FORM_NAMES.phone]: '',
  },
  [FORMS_KEYS.delivery]: {
    [FORM_NAMES.deliveryCity]: '',
    [FORM_NAMES.deliveryDepartment]: '',
  },
  [FORMS_KEYS.payment]: {
    [FORM_NAMES.paymentMethod]: '',
  },
};

export const formSteps = {
  [FORMS_KEYS.personalData]: 0,
  [FORMS_KEYS.delivery]: 1,
  [FORMS_KEYS.payment]: 2,
};

export const formsMap = [
  {
    title: 'Ваші дані',
    formKey: FORMS_KEYS.personalData,
    Form: (props) => <UserDataForm {...props} />,
  },
  {
    title: 'Доставка',
    formKey: FORMS_KEYS.delivery,
    Form: (props) => <DeliveryForm {...props} />,
  },
  {
    title: 'Оплата',
    formKey: FORMS_KEYS.payment,
    Form: (props) => <PaymentForm {...props} />,
  },
];
