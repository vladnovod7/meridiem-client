import React, { FC, useCallback, useState, useEffect } from 'react';
import { Steps } from 'antd';
import { useUser, useUserPersonalData } from '@hooks';
import { FormCollapseWrapper } from './_components';
import { initialFormsState, formSteps, FORMS_KEYS, formsMap } from './_constants';
import { isDataPrefilled, mapDescription, preFillFormsData, getCurrentStep, getPlainFormData } from './_utils';
import { isEmpty } from 'lodash';
import { UserPersonalData } from '@interfaces';

const { Step } = Steps;

const Checkout: FC = () => {
  const [currentStep, setCurrentStep] = useState(0);
  const [formsData, setFormsData] = useState<Record<string, UserPersonalData>>(initialFormsState);

  const { data: personalData, addData } = useUserPersonalData();

  const handleSetFormData = useCallback((formKey, data) => {
    setFormsData((prev) => ({ ...prev, [formKey]: data }));
  }, []);

  const handleFormSubmit = useCallback(
    (formKey: string) => (data: Record<string, string>) => {
      handleSetFormData(formKey, data);
    },
    [handleSetFormData],
  );

  const getDescription = useCallback(
    (formKey: string) =>
      isDataPrefilled(formsData[formKey]) ? mapDescription(formKey, Object.values(formsData[formKey])) : '',
    [formsData],
  );

  const getIsClosed = useCallback((formKey) => currentStep !== formSteps[formKey], [currentStep]);

  useEffect(() => {
    if (!isEmpty(personalData)) setFormsData(preFillFormsData(personalData));
  }, [handleSetFormData, personalData]);

  useEffect(() => {
    const newCurrentStep = getCurrentStep(formsData);

    setCurrentStep(newCurrentStep);

    if (newCurrentStep === Infinity) {
      addData(getPlainFormData(formsData));
    }
  }, [addData, formsData]);

  return (
    <Steps direction="vertical" current={currentStep}>
      {formsMap.map(({ title, Form, formKey }) => (
        <Step
          key={title}
          title={title}
          description={
            <FormCollapseWrapper isClosed={getIsClosed(formKey)} description={getDescription(formKey)}>
              <Form data={formsData[formKey]} onSubmit={handleFormSubmit(formKey)} />
            </FormCollapseWrapper>
          }
        />
      ))}
    </Steps>
  );
};

export default Checkout;
