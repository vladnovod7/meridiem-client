import styled, { css } from 'styled-components';

export const DescriptionWrapper = styled.div`
  max-height: 40px;
  overflow: hidden;
  transition: max-height 0.2s;
`;

export const FormWrapper = styled.div`
  overflow: hidden;
  transition: max-height 0.2s;
  max-height: 1000px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;

  ${DescriptionWrapper} {
    max-height: 0px;
  }

  ${({ isClosed }) =>
    isClosed &&
    css`
      ${DescriptionWrapper} {
        max-height: 40px;
      }

      ${FormWrapper} {
        max-height: 0px;
      }
    `}
`;
