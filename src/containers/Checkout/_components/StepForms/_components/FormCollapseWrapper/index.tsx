import React, { FC } from 'react';
import * as Styled from './styles';

interface IFormCollapseWrapper {
  isClosed: boolean;
  description?: string;
}

const FormCollapseWrapper: FC<IFormCollapseWrapper> = ({ isClosed, description, children }: IFormCollapseWrapper) => {
  console.log(isClosed);

  return (
    <Styled.Wrapper isClosed={isClosed}>
      <Styled.DescriptionWrapper>{description}</Styled.DescriptionWrapper>
      <Styled.FormWrapper>{children}</Styled.FormWrapper>
    </Styled.Wrapper>
  );
};

export default FormCollapseWrapper;
