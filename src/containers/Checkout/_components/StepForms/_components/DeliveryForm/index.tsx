import React, { FC, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Button } from 'antd';
import { InputRHF } from '@ui-kit';
import { validationSchema } from './_constants';
import { FORM_NAMES } from '../../_constants';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Styled from './styles';

interface IUserDataFromProps {
  data: Record<string, string>;
  onSubmit(data: Record<string, string>): void;
}

const UserDataForm: FC<IUserDataFromProps> = ({ data, onSubmit }: IUserDataFromProps) => {
  const { control, handleSubmit, reset } = useForm({
    mode: 'onChange',
    resolver: yupResolver(validationSchema),
    defaultValues: data,
  });

  useEffect(() => {
    reset(data);
  }, [data, reset]);

  return (
    <Styled.Wrapper onSubmit={handleSubmit(onSubmit)}>
      <InputRHF size="large" control={control} name={FORM_NAMES.deliveryCity} placeholder="Місто" />
      <InputRHF
        size="large"
        control={control}
        name={FORM_NAMES.deliveryDepartment}
        placeholder="Відділення Нової Пошти"
      />
      <Button htmlType="submit" size="large" type="primary">
        Далі
      </Button>
    </Styled.Wrapper>
  );
};

export default UserDataForm;
