import * as yup from 'yup';
import { FORM_NAMES } from '../../_constants';

export const validationSchema = yup.object().shape({
  [FORM_NAMES.deliveryCity]: yup.string().required("Обов'язкове поле").trim(),
  [FORM_NAMES.deliveryDepartment]: yup.string().required("Обов'язкове поле"),
});
