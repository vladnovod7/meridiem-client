import React, { FC, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Button } from 'antd';
import { RadioGroupRHF } from '@ui-kit';
import { PAYMENT_METHODS } from './_constants';
import { FORM_NAMES } from '../../_constants';
import * as Styled from './styles';

interface IPaymentForm {
  data: Record<string, string>;
  onSubmit(data: Record<string, string>): void;
}

const PaymentForm: FC<IPaymentForm> = ({ data, onSubmit }: IPaymentForm) => {
  const { control, handleSubmit, reset } = useForm({
    mode: 'onChange',
    defaultValues: data,
  });

  useEffect(() => {
    reset(data);
  }, [data, reset]);

  return (
    <Styled.Wrapper onSubmit={handleSubmit(onSubmit)}>
      <RadioGroupRHF
        size="large"
        control={control}
        name={FORM_NAMES.paymentMethod}
        options={PAYMENT_METHODS}
        defaultValue={PAYMENT_METHODS[0].value}
      />
      <Button htmlType="submit" size="large" type="primary">
        Далі
      </Button>
    </Styled.Wrapper>
  );
};

export default PaymentForm;
