export const PAYMENT_METHODS = [
  {
    label: 'Оплата в відділенні',
    value: 'in_department',
  },
  {
    label: 'Переказ на розрахунковий рахунок',
    value: 'direct_paymaent',
  },
];
