import * as yup from 'yup';
import { FORM_NAMES } from '../../_constants';

export const validationSchema = yup.object().shape({
  [FORM_NAMES.name]: yup.string().required("Обов'язкове поле").trim().matches(/ /, "Має містити ім'я та прізвище"),
  [FORM_NAMES.email]: yup.string().required("Обов'язкове поле").trim().email('Неправильна пошта'),
  [FORM_NAMES.phoneNumber]: yup.string().required("Обов'язкове поле").trim(),
});
