import styled from 'styled-components';
import { StyledInputWrapper } from '@ui-kit';

export const Wrapper = styled.form`
  ${StyledInputWrapper} {
    margin-bottom: 10px;
  }
`;
