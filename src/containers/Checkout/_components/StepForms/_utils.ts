import { UserPersonalData } from '@interfaces';
import { FORMS_KEYS, initialFormsState, formSteps } from './_constants';
import { PAYMENT_METHODS } from './_components/PaymentForm/_constants';
import { pick } from 'lodash';

export const isDataPrefilled = (data: UserPersonalData): boolean =>
  Object.values(data).every((value) => !!value?.length);

export const mapDescription = (formKey: string, data: string[]): string => {
  if (formKey === FORMS_KEYS.payment) {
    return PAYMENT_METHODS.find(({ value }) => value === data[0])?.label || '';
  }

  return data.join(', ');
};

export const preFillFormsData = (personalData: UserPersonalData): Record<string, UserPersonalData> => {
  return Object.entries(initialFormsState).reduce((acc, [formKey, data]) => {
    const dataKeys = Object.keys(data);

    const prefilledFormData = pick(personalData, dataKeys);

    return {
      ...acc,
      [formKey]: {
        ...data,
        ...prefilledFormData,
      },
    };
  }, {});
};

export const getCurrentStep = (formsData: Record<string, UserPersonalData>): number => {
  const currentFormKey = Object.entries(formsData).find(([, data]) =>
    Object.values(data).some((value) => !value?.length),
  )?.[0];

  return currentFormKey ? formSteps[currentFormKey] : Infinity;
};

export const getPlainFormData = (formsData: Record<string, UserPersonalData>): UserPersonalData => {
  return Object.values(formsData).reduce((acc, data) => ({
    ...acc,
    ...data,
  }));
};
