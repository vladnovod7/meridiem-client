export const productFilterUrl = (productId: string, queries: string): string => {
  return `/products?productId=${productId}${queries.length ? `&${queries}` : ''}`;
};
