import React, { FC } from 'react';
import { Row, Col } from 'antd';
import { LayoutContainer } from '@ui-kit';
import { FiltersBar, ProductsList } from './_components';
import * as Styled from './styles';

const Products: FC<Record<string, never>> = () => {
  return (
    <Styled.Wrapper>
      <LayoutContainer>
        <Row justify="end" gutter={[16, 5]}>
          <Col span={5}>
            <FiltersBar />
          </Col>
          <Col span={19}>
            <ProductsList />
          </Col>
        </Row>
      </LayoutContainer>
    </Styled.Wrapper>
  );
};

export default Products;
