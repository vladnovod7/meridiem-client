import React, { FC } from 'react';
import { Row, Col, Typography } from 'antd';
import { Product } from '@ui-kit';
import { useProducts, useFilters } from '../../_hooks';
import * as Styled from './styles';

const ProductsList: FC<Record<string, never>> = () => {
  const { productsTitle } = useFilters();
  const { products, handleClickOnProduct } = useProducts();

  return (
    <Styled.Wrapper>
      <Typography.Title level={2}>{productsTitle}</Typography.Title>
      <Row gutter={[5, 5]}>
        {products &&
          products.items?.map((product) => (
            <Col key={product.id} span={6}>
              <Product product={product} onTitleClick={handleClickOnProduct} />
            </Col>
          ))}
      </Row>
    </Styled.Wrapper>
  );
};

export default ProductsList;
