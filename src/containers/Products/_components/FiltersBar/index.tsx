import React, { FC } from 'react';
import { useFilters } from '../../_hooks';
import { FiltersBlock, PickedFiltersBlock } from './_components';

const FiltersBar: FC<Record<string, never>> = () => {
  const { dumpedFilterValues, filterData, handleDeletePickedData, handleFilterDataChange } = useFilters();

  return (
    <>
      {!!dumpedFilterValues.length && (
        <PickedFiltersBlock values={dumpedFilterValues} onDelete={handleDeletePickedData} />
      )}
      <FiltersBlock filterData={filterData} onChange={handleFilterDataChange} />
    </>
  );
};

export default FiltersBar;
