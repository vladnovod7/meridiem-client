import React, { FC, useMemo } from 'react';
import { Collapse } from 'antd';
import { ICatalogProperty } from '@interfaces';
import { FilterOutlined } from '@ant-design/icons';
import { FilterBlock } from './_components';
import * as Styled from './styles';

interface FilterDataType extends ICatalogProperty {
  values?: string[] | undefined;
}

interface IFiltersBlock {
  filterData?: FilterDataType[];
  onChange(data: string[], id: string): void;
}

const FiltersBlock: FC<IFiltersBlock> = ({ filterData, onChange }: IFiltersBlock) => {
  const filterIds = useMemo(() => filterData?.map(({ id }) => id) || [], [filterData]);

  return (
    !!filterData?.length && (
      <Styled.Wrapper>
        <Styled.Title>
          <FilterOutlined />
          Фільтри
        </Styled.Title>
        <Collapse defaultActiveKey={filterIds} expandIconPosition={'right'} bordered={false}>
          {filterData?.map((filterData) => (
            <Collapse.Panel forceRender header={filterData.title} key={filterData.id}>
              <FilterBlock {...filterData} onChange={onChange} />
            </Collapse.Panel>
          ))}
        </Collapse>
      </Styled.Wrapper>
    )
  );
};

export default FiltersBlock;
