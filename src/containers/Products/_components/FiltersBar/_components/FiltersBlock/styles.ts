import styled from 'styled-components';

export const Wrapper = styled.div`
  box-shadow: 0 1px 2px rgb(0 0 0 / 25%);
  padding: 8px;
  border-radius: 10px;
  margin-top: 10px;
  background: white;
`;

export const Title = styled.h3`
  padding-left: 15px;
  margin-bottom: 10px;

  > span {
    margin-right: 6px;
  }
`;
