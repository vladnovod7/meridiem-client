import styled from 'styled-components';

export const Wrapper = styled.div`
  .ant-checkbox-group {
    display: flex;
    flex-direction: column;

    .ant-checkbox-checked .ant-checkbox-inner {
      background-color: var(--primary);
      border-color: var(--primary);
    }

    label:not(:last-child) {
      margin-bottom: 6px;
    }
  }
`;

export const Title = styled.h3`
  margin-bottom: 10px;
`;
