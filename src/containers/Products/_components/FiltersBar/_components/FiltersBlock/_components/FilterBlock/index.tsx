import React, { useCallback, FC } from 'react';
import { Checkbox } from 'antd';
import { ICatalogProperty } from '@interfaces';
import * as Styled from './styles';

const { Group } = Checkbox;

interface IFilterBlockProps extends ICatalogProperty {
  values?: string[];
  onChange(data: string[], propertyId: string): any;
}

const FilterBlock: FC<IFilterBlockProps> = ({ id, types, values, onChange }: IFilterBlockProps) => {
  const handleFilterValuesChange = useCallback((newValues) => onChange(newValues, id), [id, onChange]);

  const dumpedTypes = types.map(({ id, title }) => ({ label: title, value: id }));

  return (
    <Styled.Wrapper>
      <Group options={dumpedTypes} value={values} onChange={handleFilterValuesChange} />
    </Styled.Wrapper>
  );
};

export default FilterBlock;
