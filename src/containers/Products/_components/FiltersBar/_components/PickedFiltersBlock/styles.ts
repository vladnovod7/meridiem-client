import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 8px 15px;
  box-shadow: 0 1px 2px rgb(0 0 0 / 25%);
  margin: 10px 0;
  border-radius: 10px;
  background: white;
`;

export const Title = styled.h3`
  margin-bottom: 10px;
  padding-left: 8px;

  svg {
    margin-right: 6px;
  }
`;

export const Brand = styled.span`
  color: black;
`;

export const ChipWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  > * {
    margin-bottom: 6px;
    margin-right: 6px;
  }
`;
