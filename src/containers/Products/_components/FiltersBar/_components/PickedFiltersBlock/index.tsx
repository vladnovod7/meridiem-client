import React, { FC } from 'react';
import { CheckSquareOutlined } from '@ant-design/icons';
import { Chip } from '@ui-kit';
import * as Styled from './styles';

interface IPickedFilterBlockProps {
  values: Record<string, string[]>[];
  onDelete(payload: any[]): void;
}

const PickedFiltersBlock: FC<IPickedFilterBlockProps> = ({ values, onDelete }: IPickedFilterBlockProps) => {
  return (
    <Styled.Wrapper>
      <Styled.Title>
        <CheckSquareOutlined />
        Ви обрали:
      </Styled.Title>
      <Styled.ChipWrapper>
        {values.map((valueObj) =>
          Object.entries(valueObj).map(([properyName, typeNames]) =>
            typeNames.map(
              (typeName) =>
                !!typeName?.length && (
                  <Chip key={`${properyName}_${typeName}`} payload={[properyName, typeName]} onClose={onDelete}>
                    <Styled.Brand>{properyName}:</Styled.Brand> {typeName}
                  </Chip>
                ),
            ),
          ),
        )}
      </Styled.ChipWrapper>
    </Styled.Wrapper>
  );
};

export default PickedFiltersBlock;
