import { useQuery } from 'react-query';
import { omit } from 'lodash';
import { IProduct } from '@interfaces';
import { filterQueryParser } from '@utils';
import { useRouter } from 'next/router';
import { products } from '@api';

const { fetchProducts, fetchProductsKey } = products;

interface IUseProducts {
  products: { items?: IProduct[] };
  handleClickOnProduct(productId: IProduct['id']): void;
}

const useProducts = (): IUseProducts => {
  const router = useRouter();

  const parsedQuery = filterQueryParser(router.query);

  const { data } = useQuery(
    fetchProductsKey(parsedQuery?.productId?.[0], omit(parsedQuery, ['productId'])),
    () => fetchProducts(parsedQuery.productId?.[0], omit(parsedQuery, ['productId'])),
    {
      staleTime: Infinity,
    },
  );

  const handleClickOnProduct = (productId: IProduct['id']) => {
    router.push(`/products/details?id=${productId}`);
  };

  return {
    products: data || {},
    handleClickOnProduct,
  };
};

export default useProducts;
