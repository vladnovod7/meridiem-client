import { useMemo, useCallback } from 'react';
import { omit } from 'lodash';
import { ICatalogType, ICatalogProperty, ICatalogOption } from '@interfaces';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';
import { productFilters } from '@api';
import { productFilterUrl } from '../_utils';
import { filterQueryParser, filterQueryMapper, unionObjectInnerArrays } from '@utils';

const { fetchFilters, fetchCatalogKey } = productFilters;

interface IUseFilters {
  dumpedFilterValues: Record<ICatalogProperty['title'], ICatalogType['title'][] | undefined>[];
  filterData?: {
    values?: string[] | undefined;
    id: string;
    title: string;
    types: ICatalogType[];
  }[];
  handleDeletePickedData(dataArr: [ICatalogProperty['title'], ICatalogType['title']]): void;
  handleFilterDataChange(dataArr: ICatalogType['id'][], id: ICatalogProperty['id']): void;
  productsTitle?: ICatalogOption['title'];
}

const useFilters = (): IUseFilters => {
  const router = useRouter();
  const { query } = router;

  const productId = Array.isArray(query.productId) ? query.productId[0] : query.productId;

  const { data: filterOption } = useQuery(fetchCatalogKey(productId), () => fetchFilters(productId), {
    staleTime: Infinity,
  });

  const filterValues = useMemo(() => filterQueryParser(omit(query, ['productId'])), [query]);

  const filterData = filterOption?.properties.map((property) => ({
    ...property,
    ...(filterValues[property.id] ? { values: filterValues[property.id] } : {}),
  }));

  const handleFilterDataChange = useCallback(
    (data: ICatalogType['id'][], id: ICatalogProperty['id']) => {
      const dumpedData = { [id]: data };

      if ((filterValues[id]?.length || 0) < data.length) {
        const filterQueries = filterQueryMapper(unionObjectInnerArrays(dumpedData, filterValues));

        router.push(productFilterUrl(productId, filterQueries));
      } else {
        const filterQueries = filterQueryMapper({ ...filterValues, ...dumpedData });

        router.push(productFilterUrl(productId, filterQueries));
      }
    },
    [filterValues, productId, router],
  );

  const handleDeletePickedData = useCallback(
    ([propertyName, typeName]) => {
      const property = filterData?.find(({ title }) => title === propertyName);

      const propertyId = property?.id || '';

      const typeId = property?.types.find((type) => type.title === typeName)?.id;

      const filterQueries = filterQueryMapper({
        ...filterValues,
        [propertyId]: filterValues?.[propertyId]?.filter((id) => id !== typeId),
      });

      router.push(productFilterUrl(productId, filterQueries));
    },
    [filterData, filterValues, productId, router],
  );

  const dumpedFilterValues = useMemo(
    () =>
      Object.entries(filterValues).map(([propertyId, typeIds]) => {
        const property = filterData?.find(({ id }) => id === propertyId);
        const propertyTitle = property?.title || '';

        const typesMap: Record<string, string> =
          property?.types.reduce((acc, type) => ({ ...acc, [type.id]: type.title }), {}) || {};

        const typesTitles = typeIds?.map((typeId) => typesMap[typeId]);

        return { [propertyTitle]: typesTitles };
      }),
    [filterData, filterValues],
  );

  return {
    dumpedFilterValues,
    filterData,
    handleDeletePickedData,
    handleFilterDataChange,
    productsTitle: filterOption?.title,
  };
};

export default useFilters;
