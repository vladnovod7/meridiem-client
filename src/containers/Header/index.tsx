import React, { FC } from 'react';
import { Row, Col, Space } from 'antd';
import { LayoutContainer } from '@ui-kit';
import { Catalog, Search, Logo, NavButton, CartButton, Account } from './_components';
import * as Styled from './styles';

const Header: FC<Record<string, unknown>> = () => {
  return (
    <Styled.Wrapper>
      <LayoutContainer>
        <Row gutter={[16, 5]}>
          <Styled.Col span={5}>
            <Logo>MERIDIEM</Logo>
          </Styled.Col>
          <Styled.Col span={16}>
            <Space size={16}>
              <NavButton>м. Київ</NavButton>
              <NavButton>+38 (066) 709 76 14</NavButton>
            </Space>
          </Styled.Col>
          <Styled.Col span={3}>
            <Account />
          </Styled.Col>
          <Col span={5}>
            <Catalog />
          </Col>
          <Col span={16}>
            <Search />
          </Col>
          <Col span={3}>
            <CartButton />
          </Col>
        </Row>
      </LayoutContainer>
    </Styled.Wrapper>
  );
};

export default Header;
