import styled from 'styled-components';
import { Col as AntdCol } from 'antd';

export const Wrapper = styled.header`
  background: var(--primary);
  padding: 10px 0;
  position: relative;
  z-index: 2;
`;

export const Col = styled(AntdCol)`
  line-height: 50px;
`;