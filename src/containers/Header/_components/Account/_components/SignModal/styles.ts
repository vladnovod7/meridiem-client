import styled from 'styled-components';
import { Wrapper as InputWrapper } from '@ui-kit/Input/styles';

export const Wrapper = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;

  ${InputWrapper} {
    width: 100%;
    margin-bottom: 15px;
  }

  button {
    width: 100%;

    & + button {
      margin-top: 15px;
    }
  }
`;
