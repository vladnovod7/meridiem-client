import React, { FC, useCallback } from 'react';
import * as yup from 'yup';
import { isEmpty } from 'lodash';
import { Modal, Typography, Button, Divider } from 'antd';
import { InputRHF } from '@ui-kit';
import firebase, { auth } from '@lib/firebase';
import { GoogleOutlined } from '@ant-design/icons';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { ModalProps } from 'antd/lib/modal';
import { showErrorFromFirebase } from '@utils';
import * as Styled from './styles';

const provider = new firebase.auth.GoogleAuthProvider();

const schema = yup.object().shape({
  email: yup.string().required("Обов'язкове поле").email('Неправильна пошта'),
  password: yup.string().required("Обов'язкове поле").min(6, 'Пароль має бути довшим'),
});

interface IModalProps extends ModalProps {
  onCancel(): void;
}

const SignModal: FC<IModalProps> = ({ onCancel, ...rest }: IModalProps) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'onChange',
    reValidateMode: 'onChange',
  });

  const signIn = useCallback(
    async ({ email, password }) => {
      try {
        await auth.signInWithEmailAndPassword(email, password);

        onCancel();
      } catch (e) {
        showErrorFromFirebase(e);
      }
    },
    [onCancel],
  );

  const signUp = useCallback(
    async ({ email, password }) => {
      try {
        await auth.createUserWithEmailAndPassword(email, password);

        onCancel();
      } catch (e) {
        showErrorFromFirebase(e);
      }
    },
    [onCancel],
  );

  const signInWithGoogle = useCallback(async () => {
    await firebase.auth().signInWithPopup(provider);

    onCancel();
  }, [onCancel]);

  return (
    <Modal width={360} {...rest} onCancel={onCancel}>
      <Styled.Wrapper>
        <Typography.Title level={2}>Вхід</Typography.Title>
        <InputRHF name="email" size="large" control={control} />
        <InputRHF name="password" size="large" type="password" control={control} />
        <Button disabled={!isEmpty(errors)} type="primary" size="large" onClick={handleSubmit(signIn)}>
          Увійти
        </Button>
        <Divider>Або</Divider>
        <Button disabled={!isEmpty(errors)} type="primary" size="large" onClick={handleSubmit(signUp)}>
          Зареєструватись
        </Button>
        <Divider />
        <Button size="large" icon={<GoogleOutlined />} onClick={signInWithGoogle}>
          Google
        </Button>
      </Styled.Wrapper>
    </Modal>
  );
};

export default SignModal;
