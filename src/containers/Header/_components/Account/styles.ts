import styled from 'styled-components';

export const Label = styled.span`
  color: white;
  font-weight: 500;
  padding: 6px 0;
  cursor: pointer;
`;

export const Option = styled.div`
  font-weight: 700;
  width: 100%;
  text-align: center;
  padding: 10px 0;
  transition: background 0.2s;
  border-radius: 6px;
  cursor: pointer;

  &:hover {
    background: var(--secondary);
  }
`;

export const OptionsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background: white;
  width: 170px;
  border-radius: 10px;
  align-items: center;
  box-shadow: 0 1px 2px rgb(0 0 0 / 25%);

  &:not(:last-child) {
    border-bottom: 1px solid lightgray;
  }
`;
