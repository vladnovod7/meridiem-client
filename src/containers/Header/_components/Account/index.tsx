import React, { FC, useState, useCallback } from 'react';
import { Dropdown } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { auth } from '@lib/firebase';
import { useUser } from '@hooks';
import { SignModal } from './_components';
import * as Styled from './styles';

const Account: FC<Record<string, never>> = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const user = useUser();

  const handleLogoutClick = useCallback(() => {
    auth.signOut();
  }, []);

  const handleSignIn = useCallback(() => {
    setModalVisible(true);
  }, []);

  const handleCancelModal = useCallback(() => {
    setModalVisible(false);
  }, []);

  return (
    <>
      <Dropdown
        trigger={['click', 'hover']}
        disabled={!user}
        placement="bottomCenter"
        overlay={
          <Styled.OptionsWrapper>
            <Styled.Option>Кабінет</Styled.Option>
            <Styled.Option onClick={handleLogoutClick}>Вийти</Styled.Option>
          </Styled.OptionsWrapper>
        }
      >
        {user ? (
          <Styled.Label>Аккаунт</Styled.Label>
        ) : (
          <Styled.Label onClick={handleSignIn}>
            <UserOutlined /> Увійти
          </Styled.Label>
        )}
      </Dropdown>
      <SignModal centered destroyOnClose visible={modalVisible} footer={null} onCancel={handleCancelModal} />
    </>
  );
};

export default Account;
