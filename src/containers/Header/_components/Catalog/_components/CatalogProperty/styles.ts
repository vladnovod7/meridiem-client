import styled from 'styled-components';
import { Typography } from 'antd';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 4px;
  max-width: 200px;
`;

export const ProperyTitle = styled(Typography.Text)`
  font-weight: 900;
  margin-bottom: 6px;
  padding: 2px 6px;
`;

export const Type = styled(Typography.Text)`
  display: block;
  padding: 6px 6px;
  cursor: pointer;
  border-radius: 4px;
  background-color: var(--lightGray);
  margin: 0 2px 2px 0;

  &:hover {
    background-color: var(--secondary);
  }
`;

export const TypesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
