import React, { useCallback } from 'react';
import { ICatalogProperty, ICatalogType } from '@interfaces';
import { useCatalog } from '../CatalogOption/_context';
import * as Styled from './styles';

interface ICalogPropertyProps {
  property: ICatalogProperty;
}

export default function CatalogProperty({ property }: ICalogPropertyProps) {
  const { optionId, onTypeClick } = useCatalog();

  const handleTypeClick = useCallback(
    (type: ICatalogType) => () =>
      onTypeClick({
        productId: optionId,
        propertyId: property.id,
        typeId: type.id,
      }),
    [onTypeClick, optionId, property.id],
  );

  return (
    <Styled.Wrapper>
      <Styled.ProperyTitle>{property.title}</Styled.ProperyTitle>
      <Styled.TypesWrapper>
        {property.types.map((type) => (
          <Styled.Type key={type.id} onClick={handleTypeClick(type)}>
            {type.title}
          </Styled.Type>
        ))}
      </Styled.TypesWrapper>
    </Styled.Wrapper>
  );
}
