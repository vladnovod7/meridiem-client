import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  max-height: 50vh;
  width: 640px;
  overflow: auto;
`;
