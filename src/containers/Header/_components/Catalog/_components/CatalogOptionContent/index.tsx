import React from 'react';
import { ICatalogProperty } from '@interfaces';
import CatalogProperty from '../CatalogProperty/index';
import * as Styled from './styles';

interface ICatalogOptionContentProps {
  properties: ICatalogProperty[];
}

export default function CatalogOptionContent({ properties }: ICatalogOptionContentProps) {
  return (
    <Styled.Wrapper>
      {properties.map((property) => (
        <CatalogProperty key={property.id} property={property} />
      ))}
    </Styled.Wrapper>
  );
}
