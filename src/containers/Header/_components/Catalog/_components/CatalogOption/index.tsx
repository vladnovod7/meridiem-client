import React, { useMemo, useCallback } from 'react';
import { useRouter } from 'next/router';
import { Popover } from 'antd';
import { ICatalogOption, IFetchProductsQueries } from '@interfaces';
import { filterQueryMapper } from '@utils';
import CatalogOptionContent from '../CatalogOptionContent';
import { CatalogContext } from './_context';
import * as Styled from './styles';

interface ICatalogOptionProps {
  option: ICatalogOption;
}

export default function CatalogOption({ option }: ICatalogOptionProps) {
  const router = useRouter();

  const handleCatalogTypeClick = useCallback(
    ({ productId, typeId, propertyId }: IFetchProductsQueries) => {
      router.push(`/products?productId=${productId}&${filterQueryMapper({ [propertyId]: [typeId] })}`);
    },
    [router],
  );

  const handleOptionClick = useCallback(() => {
    router.push(`/products?productId=${option.id}`);
  }, [option.id, router]);

  const contextValue = useMemo(
    () => ({
      onTypeClick: handleCatalogTypeClick,
      optionId: option.id,
    }),
    [handleCatalogTypeClick, option.id],
  );

  return (
    <CatalogContext.Provider value={contextValue}>
      <Popover content={() => <CatalogOptionContent properties={option.properties} />} placement="rightTop">
        <Styled.Title onClick={handleOptionClick}>{option.title}</Styled.Title>
      </Popover>
    </CatalogContext.Provider>
  );
}
