import styled from 'styled-components';

export const Title = styled.span`
  font-weight: 700;
  width: 100%;
  text-align: center;
  padding: 10px 0;
  -webkit-transition: background 0.2s;
  transition: background 0.2s;
  border-radius: 6px;
  cursor: pointer;
  transition: background 0.2s;

  &:hover {
    background: var(--secondary);
  }
`;
