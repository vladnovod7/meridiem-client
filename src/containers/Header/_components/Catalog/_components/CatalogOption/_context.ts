import { createContext, useContext } from 'react';
import { ICatalogOption, IFetchProductsQueries } from '@interfaces';

export interface ICatalogContext {
  onTypeClick(queries : IFetchProductsQueries): void,
  optionId: ICatalogOption["id"],
}

export const CatalogContext = createContext<ICatalogContext>({
  onTypeClick: () => {},
  optionId: '',
});

export const useCatalog = () => {
  return useContext(CatalogContext)};