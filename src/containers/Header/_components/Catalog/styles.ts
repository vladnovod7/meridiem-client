import styled from 'styled-components';
import { Button } from '@ui-kit';
import { Menu as MenuKit } from 'antd';

export const Menu = styled(MenuKit)`
  border: 1px solid white;
  border-radius: 8px;

  .ant-menu-submenu-title {
    font-weight: 700;
    height: 34px !important;
    line-height: 34px !important;
  }
`;

export const CatalogButton = styled(Button)`
  border: 1px solid white;
  width: 100%;
  text-align: left;
  margin-bottom: 6px;

  > span {
    color: white;
  }
`;

export const Catalog = styled.div`
  display: flex;
  flex-direction: column;
  background: white;
  border-radius: 8px;
  position: relative;
  box-shadow: 0px 0px 0px 100vw rgba(0, 0, 0, 0.6) !important;
  z-index: 5;
`;

export const MenuWrapper = styled.div``;

export const CatalogOption = styled.p`
  &:hover {
    color: var(--primary);
  }
`;

export const Wrapper = styled.div`
  height: 40px;
  max-height: 40px;
`;
