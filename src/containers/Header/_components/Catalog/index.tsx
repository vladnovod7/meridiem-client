import React, { useState, useEffect, useCallback } from 'react';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';
import { productFilters } from '@api';
import { CatalogOption } from './_components';
import { AppstoreOutlined } from '@ant-design/icons';
import * as Styled from './styles';

const { fetchCatalog, fetchCatalogKey } = productFilters;

export default function Catalog() {
  const [catalogVisible, setCatalogVisible] = useState(false);

  const router = useRouter();

  const { data } = useQuery(fetchCatalogKey(), fetchCatalog);

  const handleOpenCatalog = useCallback(() => setCatalogVisible(true), []);
  const handleCatalogOut = useCallback(() => setCatalogVisible(false), []);

  useEffect(() => {
    setCatalogVisible(false);
  }, [router.query]);

  return (
    <Styled.Wrapper>
      <Styled.MenuWrapper onMouseOver={handleOpenCatalog} onMouseLeave={handleCatalogOut}>
        <Styled.CatalogButton type="primary" size="large" icon={<AppstoreOutlined />}>
          Каталог
        </Styled.CatalogButton>
        {catalogVisible && (
          <Styled.Catalog>
            {data && data.map((option) => <CatalogOption key={option.id} option={option} />)}
          </Styled.Catalog>
        )}
      </Styled.MenuWrapper>
    </Styled.Wrapper>
  );
}
