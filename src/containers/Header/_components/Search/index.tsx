import React, { useState, useMemo, FC } from 'react';
import { useQuery } from 'react-query';
import { AutoComplete, Input } from 'antd';
import { products } from '@api';
import { ProductItem } from './_components';
import * as Styled from './styles';

const { searchProducts, searchProductsKeyCreator } = products;

const Search: FC<Record<string, never>> = () => {
  const [searchValue, setSearchValue] = useState('');

  const { data } = useQuery(searchProductsKeyCreator(searchValue), async () => await searchProducts(searchValue), {
    staleTime: Infinity,
  });

  const handleSearch = (value: string) => {
    setSearchValue(value);
  };

  const onSelect = () => {
    setSearchValue('');
  };

  const options = useMemo(
    () => data?.map((product) => ({ label: <ProductItem product={product} />, value: product.name })) || [],
    [data],
  );

  return (
    <Styled.Wrapper>
      <AutoComplete
        value={searchValue}
        dropdownClassName="search-dropdown"
        options={options}
        onSelect={onSelect}
        onSearch={handleSearch}
      >
        <Input size="large" placeholder="Шукай тут" />
      </AutoComplete>
    </Styled.Wrapper>
  );
};

export default Search;
