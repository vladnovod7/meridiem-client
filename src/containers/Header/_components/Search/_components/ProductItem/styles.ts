import styled from 'styled-components';

export const Title = styled.span`
  color: var(--primary);
`;

export const Image = styled.img`
  width: 40px;
  height: 40px;
  object-fit: contain;
  object-position: center;
  margin-right: 6px;
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;
