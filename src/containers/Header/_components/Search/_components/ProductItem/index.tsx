import React, { FC } from 'react';
import { IProduct } from '@interfaces';
import * as Styled from './styles';

interface IProductItemProps {
  product: IProduct;
}

const ProductItem: FC<IProductItemProps> = ({ product }: IProductItemProps) => {
  return (
    <Styled.Wrapper>
      <Styled.Image src={product.image} />
      <Styled.Title>{product.name}</Styled.Title>
    </Styled.Wrapper>
  );
};

export default ProductItem;
