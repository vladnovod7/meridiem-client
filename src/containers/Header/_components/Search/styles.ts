import styled from 'styled-components';
import { Input as InputKit } from 'antd';

export const Input = styled(InputKit)`
  border-radius: 8px;
  border: 1px solid #f0f0f0;
`;

export const Wrapper = styled.div`
  height: 100%;

  .ant-select-dropdown.search-dropdown {
    .ant-select-item.ant-select-item-option.ant-select-item-option-active {
      background-color: white !important;
      opacity: 0.2;
    }
  }

  .ant-select {
    display: block;
    height: 100%;
    width: 100%;
  }
`;
