import React, { useCallback } from 'react';
import { useRouter } from 'next/router';
import { Row, Col, Button } from 'antd';
import * as Styled from './styles';

const BuyButton = ({ price }) => {
  const router = useRouter();

  const handleNavigateToCheckout = useCallback(() => {
    router.push('checkout');
  }, [router]);

  return (
    <Styled.Wrapper>
      <Row gutter={16}>
        <Col span={12}>
          <Button type="primary" size="large" onClick={handleNavigateToCheckout}>
            Оформити замовлення
          </Button>
        </Col>
        <Col span={12}>
          <Styled.Price level={2}>{price}</Styled.Price>
        </Col>
      </Row>
    </Styled.Wrapper>
  );
};

export default BuyButton;
