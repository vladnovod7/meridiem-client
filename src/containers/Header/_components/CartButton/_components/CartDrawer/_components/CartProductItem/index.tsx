import React, { FC, useCallback } from 'react';
import { ICartProduct } from '@interfaces';
import { Counter } from '@ui-kit';
import { useCart } from '@hooks';
import * as Styled from './styles';

const CartProductItem: FC<ICartProduct> = ({ id, image, name, price, count }: ICartProduct) => {
  const { updateCount } = useCart({ enabled: false });

  const handleIncrement = useCallback(() => {
    updateCount(id, count + 1);
  }, [count, id, updateCount]);

  const handleDecrement = useCallback(() => {
    updateCount(id, count - 1);
  }, [count, id, updateCount]);

  return (
    <Styled.Wrapper>
      <Styled.Image src={image} />
      <Styled.ProductDescWrapper>
        <Styled.Title>{name}</Styled.Title>
        <Styled.PriceWrapper>
          <Styled.Price>{price}</Styled.Price>
          <Styled.MultiplierWrapper>
            <Counter value={count} onIncrement={handleIncrement} onDecrement={handleDecrement} />
            <Styled.Price>{parseInt(price, 10) * count}</Styled.Price>
          </Styled.MultiplierWrapper>
        </Styled.PriceWrapper>
      </Styled.ProductDescWrapper>
    </Styled.Wrapper>
  );
};

export default CartProductItem;
