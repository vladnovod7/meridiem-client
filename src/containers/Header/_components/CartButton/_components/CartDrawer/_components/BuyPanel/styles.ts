import styled from 'styled-components';
import { Typography } from 'antd';

export const Wrapper = styled.div`
  padding: 20px 0 0 0;
  width: 100%;

  button {
    width: 100%;
  }
`;

export const Price = styled(Typography.Title)`
  align-self: flex-start;
  font-size: 24px;
  font-weight: 500;
  margin-bottom: 0 !important;

  &::after {
    font-size: 12px;
    margin-left: 4px;
    content: 'грн';
  }
`;
