import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 120px;
  width: 100%;
  padding: 15px;
  border-radius: 10px;
  border: 1px solid var(--primary);
  display: flex;

  margin-bottom: 10px;
`;

export const Image = styled.img`
  height: 100%;
  width: 88px;
  flex-shrink: 0;
  object-fit: contain;
  object-position: center;
`;

export const Title = styled.h3`
  margin-bottom: 10px;
`;

export const ProductDescWrapper = styled.div`
  height: 100%;
  width: 100%;
  padding-left: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const PriceWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Price = styled.p`
  align-self: flex-start;
  font-size: 24px;
  font-weight: 500;

  &::after {
    font-size: 12px;
    margin-left: 4px;
    content: 'грн';
  }
`;

export const MultiplierWrapper = styled.div`
  display: flex;
  align-items: center;

  ${Price} {
    margin-left: 20px;
  }
`;
