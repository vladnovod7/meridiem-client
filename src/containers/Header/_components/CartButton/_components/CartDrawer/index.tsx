import React, { FC } from 'react';
import { Drawer, Typography } from 'antd';
import { ICartProduct } from '@interfaces';
import { CartProductItem, BuyPanel } from './_components';
import { DrawerProps } from 'antd/lib/drawer';
import * as Styled from './styles';

interface ICartDrawerProps extends DrawerProps {
  products?: ICartProduct[];
}

const CartDrawer: FC<ICartDrawerProps> = ({ products, ...rest }: ICartDrawerProps) => {
  const totalPrice = products?.reduce((acc, { price, count }) => acc + parseInt(price, 10) * count, 0);

  return (
    <Drawer width={600} {...rest}>
      <Styled.Wrapper>
        <Typography.Title>Корзина</Typography.Title>
        <Styled.ProductsWrapper>
          {products?.map((product) => (
            <CartProductItem key={product.id} {...product} />
          ))}
        </Styled.ProductsWrapper>
        <BuyPanel price={totalPrice} />
      </Styled.Wrapper>
    </Drawer>
  );
};

export default CartDrawer;
