import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 100%;
  min-height: 100%;
`;

export const ProductsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  overflow: auto;
`;
