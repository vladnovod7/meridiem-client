import React, { FC, useState, useCallback } from 'react';
import { Badge } from 'antd';
import { useCart } from '@hooks';
import { ButtonProps } from 'antd/lib/button';
import { ShoppingCartOutlined } from '@ant-design/icons';
import { CartDrawer } from './_components';
import * as Styled from './styles';

const CartButton: FC<ButtonProps> = (props: ButtonProps) => {
  const [drawerOpened, setDrawerOpened] = useState<boolean>(false);

  const { data } = useCart();

  const handleButtonClick = useCallback(() => setDrawerOpened(true), []);
  const handleCloseDrawer = useCallback(() => setDrawerOpened(false), []);

  return (
    <Badge
      count={data?.length}
      style={{ backgroundColor: 'var(--primaryYellow)', borderColor: 'var(--primaryYellow)' }}
    >
      <Styled.CartButton
        type="primary"
        size="large"
        icon={<ShoppingCartOutlined />}
        onClick={handleButtonClick}
        {...props}
      >
        Кошик
      </Styled.CartButton>
      <CartDrawer products={data} visible={drawerOpened} onClose={handleCloseDrawer} />
    </Badge>
  );
};

export default CartButton;
