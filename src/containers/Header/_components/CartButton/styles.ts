import styled from 'styled-components';
import { Button } from '@ui-kit';

export const CartButton = styled(Button)`
  border-color: white;
  > span {
    color: white;
  }
`;
