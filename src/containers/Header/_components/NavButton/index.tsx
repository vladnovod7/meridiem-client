import React from 'react';
import { HtmlElement } from '@interfaces';
import * as Styled from './styles';

export default function NavButton(props: HtmlElement) {
  return <Styled.NavButton {...props} />
}