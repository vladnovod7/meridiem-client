import styled from 'styled-components';

export const NavButton = styled.span`
  color: white;
  font-weight: 700;
`;