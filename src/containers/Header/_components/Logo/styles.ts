import styled from 'styled-components';

export const Logo = styled.span`
  font-size: 30px;
  color: white;
  font-weight: 900;
`;
