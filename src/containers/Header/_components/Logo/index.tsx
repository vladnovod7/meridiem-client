import React from 'react';
import { HtmlElement } from '@interfaces';
import * as Styled from './styles';
import Link from 'next/link';

export default function Logo(props: HtmlElement) {
  return (
    <Link href="/">
      <Styled.Logo {...props} />
    </Link>
  );
}
