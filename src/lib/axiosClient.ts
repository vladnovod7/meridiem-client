import axios from 'axios';

const axiosClient = axios.create();

axiosClient.interceptors.response.use(function (response) {
  return Promise.resolve(response.data);
});

export default axiosClient;