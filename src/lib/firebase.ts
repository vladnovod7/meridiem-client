import { createContext, useState, useEffect } from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

export const config = {
  apiKey: 'AIzaSyDO3MchVf_xEHTyM2PzHw_zocVexwxgV9w',
  authDomain: 'meridiem-f9afc.firebaseapp.com',
  projectId: 'meridiem-f9afc',
  storageBucket: 'meridiem-f9afc.appspot.com',
  messagingSenderId: '557016933555',
  appId: '1:557016933555:web:1a0bdd5d0731d5c2293cc6',
  measurementId: 'G-9E3SS10E0Y',
};

try {
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }
} catch (e) {
  console.log(e);
}

export default firebase;

export const auth = firebase.auth();
export const db = firebase.firestore();
export interface IAuthContextValue {
  user: firebase.User | null;
}

export const AuthContext = createContext<IAuthContextValue>({ user: null });

export const useAuthContext = (): IAuthContextValue => {
  const [user, setUser] = useState<firebase.User | null>(null);

  useEffect(() => {
    auth.onAuthStateChanged((userAuth) => {
      setUser(userAuth);

      if (userAuth) {
        const docRef = db.collection('users').doc(userAuth.uid);

        docRef.set(
          {
            email: userAuth.email,
          },
          { merge: true },
        );
      }
    });
  }, []);

  return { user };
};
