import { useCallback } from 'react';
import { useQuery, useMutation, useQueryClient } from 'react-query';
import { ICartProduct } from '@interfaces';
import { cart } from '@api';

const { addToCart, removeFromCart, keyCreator, fetchCart, updateProductCount } = cart;

interface IUseCartReturn {
  data: ICartProduct[] | undefined;
  invalidate(): void;
  add(id: string): void;
  remove(id: string): void;
  updateCount(id: string, count: number): void;
  isFetching: boolean;
  addLoading: boolean;
  removeLoading: boolean;
}

interface useCartProps {
  enabled?: boolean;
}

export const useCart = ({ enabled = true }: useCartProps = {}): IUseCartReturn => {
  const { data, isFetching } = useQuery(keyCreator(), fetchCart, { staleTime: Infinity, enabled });

  const queryClient = useQueryClient();

  const invalidate = useCallback(() => {
    queryClient.invalidateQueries(keyCreator());
  }, [queryClient]);

  const addToCartMutation = useMutation(addToCart, {
    onSuccess: invalidate,
  });

  const removeFromCartMutation = useMutation(removeFromCart, {
    onSuccess: invalidate,
  });

  const updateCountMutation = useMutation(
    ({ id, count }: { id: string; count: number }) => updateProductCount(id, count),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(keyCreator());
      },
    },
  );

  const add = useCallback((id: string) => addToCartMutation.mutate(id), [addToCartMutation]);
  const remove = useCallback((id: string) => removeFromCartMutation.mutate(id), [removeFromCartMutation]);
  const updateCount = useCallback(
    (id: string, count: number) => updateCountMutation.mutate({ id, count }),
    [updateCountMutation],
  );

  return {
    data,
    add,
    remove,
    updateCount,
    invalidate,
    isFetching,
    addLoading: addToCartMutation.isLoading,
    removeLoading: removeFromCartMutation.isLoading,
  };
};
