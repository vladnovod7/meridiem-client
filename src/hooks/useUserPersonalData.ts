import { useState, useEffect, useCallback } from 'react';
import { UserPersonalData } from '@interfaces';
import { useUser } from '@hooks';
import { db } from '@lib/firebase';

interface IUseUserPersonalDataReturn {
  data: UserPersonalData;
  addData(data: UserPersonalData): void;
}

export const useUserPersonalData = (): IUseUserPersonalDataReturn => {
  const [personalData, setPersonalData] = useState({});
  const user = useUser();

  useEffect(() => {
    if (user) {
      const docRef = db.collection('users').doc(user.uid);

      docRef.get().then((doc) => {
        console.log(doc);
        setPersonalData(doc.data() || {});
      });
    }
  }, [user]);

  const addData = useCallback(
    (data: UserPersonalData) => {
      if (user) {
        const docRef = db.collection('users').doc(user.uid);

        docRef.set(
          {
            ...data,
          },
          { merge: true },
        );
      }
    },
    [user],
  );

  return { data: personalData, addData };
};
