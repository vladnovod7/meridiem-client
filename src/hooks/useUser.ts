import { useContext } from 'react';
import { IAuthContextValue, AuthContext } from '@lib/firebase';

export const useUser = (): IAuthContextValue['user'] => useContext(AuthContext).user;
