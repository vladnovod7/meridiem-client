import axiosClient from '@lib/axiosClient';
import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

axiosClient.defaults.baseURL = `${publicRuntimeConfig.API_PREFIX}`;

export * as products from './products';
export * as productFilters from './productFilters';
export * as cart from './cart';

export { productsRoutes } from './products/mock';
export { productFiltersRoutes } from './productFilters/mock';
export { cartRoutes } from './cart/mock';

export { default as createMockServer } from './mock';
