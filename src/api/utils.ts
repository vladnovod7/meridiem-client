import { IQueryObject } from '@interfaces';

export const createQueryObj = (
  keyCreator: IQueryObject['keyCreator'],
  request: IQueryObject['request'],
): IQueryObject => ({
  keyCreator,
  request,
});
