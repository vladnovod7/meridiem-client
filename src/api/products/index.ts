import axiosClient from '@lib/axiosClient';
import { filterQueryMapper } from '@utils';
import { IProduct, IFetchProductsQueries } from '@interfaces';
import qs from 'query-string';

export const apiKey = 'products';

export interface FetchProductsResponse {
  items: IProduct[];
}

export type SearchProductsResponse = IProduct[];

export interface IFetchProductsArgs {
  productId?: string;
  propertiesMap?: Record<string, string[] | undefined>;
}

export const fetchProductsKey = (...args: any[]): any[] => [apiKey, ...args];

export const fetchProducts = (
  productId: IFetchProductsArgs['productId'],
  propertiesMap: IFetchProductsArgs['propertiesMap'],
): Promise<FetchProductsResponse> => {
  const propertiesQuery = filterQueryMapper(propertiesMap);

  const query = `${qs.stringify({ productId })}${propertiesQuery.length ? `&${propertiesQuery}` : ''}`;

  return axiosClient.get<FetchProductsResponse>(`${apiKey}?${query}`);
};

export const searchProductsKeyCreator = (searchValue: string): string[] => [apiKey, searchValue];

export const searchProducts = (searchQuery: string): Promise<SearchProductsResponse> =>
  axiosClient.get<SearchProductsResponse>(
    `${apiKey}/search?${qs.stringify(searchQuery.length ? { query: searchQuery } : {})}`,
  );
