import { apiKey } from './index';
import { isEmpty, omit } from 'lodash';
import { filterQueryParser } from '@utils';
import xiaomiProjImage from '@assets/mocks/products/xiaomi-projector.jpg';

export const productsRoutes = (instance): any => {
  instance.db.loadData({
    products,
  });

  instance.get(`api/${apiKey}`, (schema, { queryParams }) => {
    const productProperties = omit(filterQueryParser(queryParams), ['productId']);

    if (isEmpty(productProperties)) return { items: schema.db.products };

    return {
      items: schema.db.products.filter(({ properties }) =>
        properties.some(({ id, types }) => productProperties[id]?.includes(types[0].id)),
      ),
    };
  });

  instance.get(`api/${apiKey}/search`, (schema, { queryParams }) => {
    const { query } = filterQueryParser(queryParams);

    return schema.db.products.filter(({ name }) => name.includes(query));
  });
};

const products = [
  {
    id: 1,
    image: xiaomiProjImage,
    name: 'Xiaomi miija compact projector',
    price: '420',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '1',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '2',
            title: 'LED',
          },
        ],
      },
    ],
  },
  {
    id: 2,
    image: 'https://www.dropbox.com/s/ood8rq0aug8tjr9/18955664.jpeg?raw=1',
    name: 'Epson EH-TW7400 White (V11H932040)',
    price: '92800',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '1',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '2',
            title: 'LED',
          },
        ],
      },
    ],
  },
  {
    id: 3,
    image: 'https://www.dropbox.com/s/qat8vah6g8hsqjx/171766937.jpeg?raw=1',
    name: 'BenQ MW809ST (9H.JGN77.13E)',
    price: '14726',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '1',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '2',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '4',
            title: '500-1000',
          },
        ],
      },
    ],
  },
  {
    id: 4,
    image: 'https://www.dropbox.com/s/m1tyvgks9pu8zet/13356453.jpg?raw=1',
    name: 'AOpen AH15 (MR.JT611.001)',
    price: '11476',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '3',
            title: '400-500',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '2',
            title: '100-150',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '1',
            title: 'HD (1280/720)',
          },
        ],
      },
    ],
  },
  {
    id: 5,
    image: 'https://www.dropbox.com/s/b0gc1iox1m1w23p/142116212.jpg?raw=1',
    name: 'Xiaomi Mijia Laser Projector 4K TV Metalic Grey (XMJGTYDS01FM)',
    price: '69000',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '2',
            title: 'Лазер',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '3',
            title: '150-200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '4',
            title: '4K (3840/2160)',
          },
        ],
      },
    ],
  },
  {
    id: 6,
    image: 'https://www.dropbox.com/s/sjbup61sxq5bvgz/21996693.jpg?raw=1',
    name: 'Acer C250i (MR.JRZ11.001)',
    price: '13780',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '2',
            title: '300-400',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '2',
            title: '100-150',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 7,
    image: 'https://www.dropbox.com/s/sjbup61sxq5bvgz/21996693.jpg?raw=1',
    name: 'Epson EB-FH52 White (V11H978040)',
    price: '36250',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '4',
            title: '3LCD',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '2',
            title: '100-150',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 8,
    image: 'https://www.dropbox.com/s/7ueqtcjcdldjt93/160841799.jpg?raw=1',
    name: 'XGIMI CC DARK KNIGHT',
    price: '34009',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '4',
            title: '500-1000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 9,
    image: 'https://www.dropbox.com/s/scgdrfwv26y0c8y/6545791_0.jpg?raw=1',
    name: 'VIEWSONIC PX701HD',
    price: '18650',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 10,
    image: 'https://www.dropbox.com/s/0aclo8zfn9ylgtl/6605805_0.jpg?raw=1',
    name: 'ACER P1260BTi (MR.JSW11.001)',
    price: '17585',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '3',
            title: 'UHP',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '4',
            title: '4K (3840/2160)',
          },
        ],
      },
    ],
  },
  {
    id: 11,
    image: 'https://www.dropbox.com/s/fm3tl2c2d3gltyh/6692865_0.jpg?raw=1',
    name: 'ASUS ZenBeam S2 WiFi White (90LJ00C2-B01070)',
    price: '15499',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '3',
            title: '400-500',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 12,
    image: 'https://www.dropbox.com/s/0is1rsa3atx81pr/%D0%91%D0%95%D0%9D%D0%9A.jpg?raw=1',
    name: 'BENQ MS560 (9H.JND77.13E)',
    price: '12215',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '2',
            title: '100-150',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 13,
    image: 'https://www.dropbox.com/s/bel83yp52si55oi/EPSON%20EB-1795F%20%28V11H796040%29.jpg?raw=1',
    name: 'EPSON EB-1795F (V11H796040)',
    price: '47850',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '4',
            title: '3LCD',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 14,
    image: 'https://www.dropbox.com/s/owm1k62dttei05b/ASUS%20P3E%20%2890LJ0070-B01120%29.jpg?raw=1',
    name: 'ASUS P3E (90LJ0070-B01120)',
    price: '18799',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '4',
            title: '500-1000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '2',
            title: 'HD (1280/768))',
          },
        ],
      },
    ],
  },
  {
    id: 15,
    image:
      'https://www.dropbox.com/s/edfrhu0clgm0l5f/Acer%20S1286Hn%20%28DLP%2C%20XGA%2C%203500%20ANSI%20lm%29.jpg?raw=1',
    name: 'Acer S1286Hn (DLP, XGA, 3500 ANSI lm)',
    price: '19010',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 16,
    image: 'https://www.dropbox.com/s/nlly3ryn7hf9pkf/Epson%20EB-680Wi%20White%20%28V11H742040%29.jpg?raw=1',
    name: 'Epson EB-680Wi White (V11H742040)',
    price: '84572',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '4',
            title: '3LCD',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '1',
            title: '<100',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '2',
            title: 'HD (1280/768))',
          },
        ],
      },
    ],
  },
  {
    id: 17,
    image: 'https://www.dropbox.com/s/tog5n2riirt2uvy/Tecro%20PJ-4090.jpg?raw=1',
    name: 'Tecro PJ-4090',
    price: '8399',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '1',
            title: '1LCD',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '2',
            title: '100-150',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 18,
    image: 'https://www.dropbox.com/s/yj0618rzvuxc6ey/Tecro%20PJ-1020.jpg?raw=1',
    name: 'Tecro PJ-1020',
    price: '5070',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '1',
            title: '1LCD',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '5',
            title: '1000-1500',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '2',
            title: '100-150',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
  {
    id: 19,
    image: 'https://www.dropbox.com/s/mtijffjjy1hz40x/Epson%20EB-536Wi%20%28V11H670040%29.jpg?raw=1',
    name: 'Epson EB-536Wi (V11H670040)',
    price: '46400',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '4',
            title: '3LCD',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '5',
            title: 'UHE',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '2',
            title: '100-150',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '2',
            title: 'HD (1280/768))',
          },
        ],
      },
    ],
  },
  {
    id: 20,
    image: 'https://www.dropbox.com/s/xdk5nd07w9a33zh/OPTOMA%20DS317e%20%28E1P1A1VBE1Z2%29.jpg?raw=1',
    name: 'OPTOMA DS317e (E1P1A1VBE1Z2)',
    price: '10430',
    properties: [
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '2',
            title: 'DLP',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '4',
            title: 'HID',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Діагональ',
        types: [
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
        ],
      },
    ],
  },
];
