import axiosClient from '@lib/axiosClient';
import { ICatalogOption, IFetchProductsQueries } from '@interfaces';

export const apiKey = 'filters';

export type FetchCatalogResponse = ICatalogOption[];

export const fetchCatalogKey = (...args: any[]) => [apiKey, ...args];
export const fetchCatalog = () => axiosClient.get<FetchCatalogResponse>(apiKey);

export const fetchFilters = (productId: IFetchProductsQueries['productId']): Promise<ICatalogOption> =>
  axiosClient.get<ICatalogOption>(`${apiKey}?productId=${productId}`);
