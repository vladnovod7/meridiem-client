import { apiKey } from './index';

export const productFiltersRoutes = (instance) => {
  instance.get(`api/${apiKey}`, (schema, request) => {
    if (request.queryParams.productId) return catalog.find((item) => item.id === request.queryParams.productId);

    return catalog;
  });
};

const catalog = [
  {
    id: 'projectors',
    title: 'Проектори',
    properties: [
      {
        id: 'manufacture',
        title: 'Виробник',
        types: [
          {
            id: '1',
            title: 'AAO',
          },
          {
            id: '2',
            title: 'Changhong',
          },
          {
            id: '3',
            title: 'Everycom',
          },
          {
            id: '4',
            title: 'JMGO',
          },
          {
            id: '5',
            title: 'LedProgector',
          },
          {
            id: '6',
            title: 'Thundeal',
          },
          {
            id: '7',
            title: 'TouYinger',
          },
          {
            id: '8',
            title: 'XGIMI',
          },
          {
            id: '9',
            title: 'Xiaomi',
          },
          {
            id: '10',
            title: 'AOpen',
          },
          {
            id: '11',
            title: 'Acer',
          },
          {
            id: '12',
            title: 'Epson',
          },
          {
            id: '13',
            title: 'VIEWSONIC',
          },
          {
            id: '14',
            title: 'BENQ',
          },
          {
            id: '15',
            title: 'OPTOMA',
          },
          {
            id: '16',
            title: 'Tecro',
          },
        ],
      },
      {
        id: 'lamp',
        title: 'Лампа',
        types: [
          {
            id: '1',
            title: 'LED',
          },
          {
            id: '2',
            title: 'Лазер',
          },
          {
            id: '3',
            title: 'UHP',
          },
          {
            id: '4',
            title: 'HID',
          },
          {
            id: '5',
            title: 'UHE',
          },
        ],
      },
      {
        id: 'dilatation',
        title: 'Розширення',
        types: [
          {
            id: '1',
            title: 'HD (1280/720)',
          },
          {
            id: '2',
            title: 'HD (1280/768)',
          },
          {
            id: '3',
            title: 'Full HD (1920/1080)',
          },
          {
            id: '4',
            title: '4K (3840/2160)',
          },
        ],
      },
      {
        id: 'matrix',
        title: 'Матриця',
        types: [
          {
            id: '1',
            title: '1LCD',
          },
          {
            id: '2',
            title: 'DLP',
          },
          {
            id: '3',
            title: 'LCoS',
          },
          {
            id: '4',
            title: '3LCD',
          },
        ],
      },
      {
        id: 'brightness',
        title: 'Яскравість',
        types: [
          {
            id: '1',
            title: '200-300',
          },
          {
            id: '2',
            title: '300-400',
          },
          {
            id: '3',
            title: '400-500',
          },
          {
            id: '4',
            title: '500-1000',
          },
          {
            id: '5',
            title: '1000-1500',
          },
          {
            id: '6',
            title: '1500-2000',
          },
          {
            id: '7',
            title: '>2000',
          },
        ],
      },
      {
        id: 'contrast',
        title: 'Контрастність',
        types: [
          {
            id: '1',
            title: '<1000:1',
          },
          {
            id: '2',
            title: '1000:1-2000:1',
          },
          {
            id: '3',
            title: '2000:1-3000:1',
          },
          {
            id: '4',
            title: '3000:1-4000:1',
          },
          {
            id: '5',
            title: '>4000',
          },
        ],
      },
      {
        id: 'correlation',
        title: 'Співвідношення сторін',
        types: [
          {
            id: '1',
            title: '16:9',
          },
          {
            id: '2',
            title: '16:10',
          },
          {
            id: '3',
            title: '4:3',
          },
        ],
      },
      {
        id: 'diagonal',
        title: 'Максимальна діагональ',
        types: [
          {
            id: '1',
            title: '<100',
          },
          {
            id: '2',
            title: '100-150',
          },
          {
            id: '3',
            title: '150-200',
          },
          {
            id: '4',
            title: '>200',
          },
        ],
      },
      {
        id: 'proectioncorrelation',
        title: 'Проекційне співвідношення',
        types: [
          {
            id: '1',
            title: 'Ультракороткофокусне (до 0.8)',
          },
          {
            id: '2',
            title: 'Короткофокусне (0.8-1.3)',
          },
          {
            id: '3',
            title: 'Стандартне (1.31-2.0)',
          },
        ],
      },
      {
        id: 'focusset',
        title: 'Налаштування фокуса',
        types: [
          {
            id: '1',
            title: 'Ручне',
          },
          {
            id: '2',
            title: 'Автоматичне',
          },
          {
            id: '3',
            title: 'Цифрове',
          },
        ],
      },
      {
        id: '3D',
        title: 'Підтримка 3D',
        types: [
          {
            id: '1',
            title: 'Є',
          },
          {
            id: '2',
            title: 'Немає',
          },
        ],
      },
      {
        id: 'trapeze',
        title: 'Корекція трапеції',
        types: [
          {
            id: '1',
            title: 'Ручна',
          },
          {
            id: '2',
            title: 'Автоматична',
          },
          {
            id: '3',
            title: 'Цифрова',
          },
          {
            id: '4',
            title: 'Немає',
          },
        ],
      },
      {
        id: 'opportunities',
        title: 'Додаткові можливості',
        types: [
          {
            id: '1',
            title: '200-300',
          },
          {
            id: '2',
            title: 'Підтримка АСЗ',
          },
          {
            id: '3',
            title: 'Провідна синхронзація екрана смартфона',
          },
          {
            id: '4',
            title: 'Безпровідна синхронзація екрана смартфона',
          },
          {
            id: '5',
            title: 'Операційна система Android',
          },
          {
            id: '6',
            title: 'Безпровідні модулі',
          },
          {
            id: '7',
            title: 'Підтримка 3D',
          },
          {
            id: '8',
            title: 'Цифровий зум',
          },
          {
            id: '9',
            title: 'Вбудований акумулятор',
          },
        ],
      },
    ],
  },
  {
    id: 'screens',
    title: 'Полотна',
    properties: [
      {
        id: 'light-behavior',
        title: 'Тип',
        types: [
          {
            id: '1',
            title: 'Звичайне полотно',
          },
          {
            id: '2',
            title: 'Світлоповертаюче полотно',
          },
          {
            id: '3',
            title: 'ALR полотно',
          },
        ],
      },
      {
        id: 'binding',
        title: 'Кріплення',
        types: [
          {
            id: '1',
            title: 'Рама',
          },
          {
            id: '2',
            title: 'Стійка',
          },
          {
            id: '3',
            title: 'Cтельове кріплення',
          },
          {
            id: '4',
            title: 'Підлоговий',
          },
        ],
      },
    ],
  },
];
