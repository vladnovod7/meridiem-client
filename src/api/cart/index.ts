import axiosClient from '@lib/axiosClient';
import { IFetchProductsQueries, ICartProduct } from '@interfaces';
import qs from 'query-string';

export const apiKey = 'cart';

export type FetchCartResponse = ICartProduct[];

interface IKeyCreatorOnUpdatePayload {
  productId: ICartProduct['id'];
  count: ICartProduct['count'];
}

export const keyCreator = (...args: any[]) => [apiKey, ...args];
export const keyCreatorOnUpdate = (payload: IKeyCreatorOnUpdatePayload) => keyCreator(payload);

export const fetchCart = (): Promise<FetchCartResponse> => axiosClient.get<FetchCartResponse>(apiKey);

export const addToCart = (productId: IFetchProductsQueries['productId']): Promise<FetchCartResponse> =>
  axiosClient.post<FetchCartResponse>(apiKey, { id: productId });

export const removeFromCart = (productId: IFetchProductsQueries['productId']): Promise<FetchCartResponse> =>
  axiosClient.delete<FetchCartResponse>(apiKey, { data: { id: productId } });

export const updateProductCount = (
  productId: ICartProduct['id'],
  count: ICartProduct['count'],
): Promise<ICartProduct> => axiosClient.patch<ICartProduct>(apiKey, { id: productId, count });
