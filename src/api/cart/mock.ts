import { apiKey } from './index';
import { IProduct } from '@interfaces';
import { products } from '@api';

const cart: IProduct[] = [];

export const cartRoutes = (instance) => {
  instance.db.loadData({
    cart,
  });

  instance.get(`api/${apiKey}`, (schema, request) => {
    return schema.db.cart;
  });

  instance.delete(`api/${apiKey}`, (schema, { requestBody }) => {
    const params = JSON.parse(requestBody);

    schema.db.cart.remove(params.id);

    return schema.db.cart;
  });

  instance.post(`api/${apiKey}`, (schema, { requestBody }) => {
    const params = JSON.parse(requestBody);

    const product = schema.db.products.findBy({ id: params.id });

    //const isProductAlreadyInCart = !!schema.db.cart.length && schema.db.cart.findBy({ id: product.id });

    //if (!isProductAlreadyInCart)
    schema.db.cart.firstOrCreate({ ...product, count: 1 });

    return schema.db.cart;
  });

  instance.patch(`api/${apiKey}`, (schema, { requestBody }) => {
    const params = JSON.parse(requestBody);

    schema.db.cart.update({ id: params.id }, { count: params.count });

    return schema.db.cart;
  });
};
