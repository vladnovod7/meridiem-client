import { createServer, Model } from 'miragejs';
import { productsRoutes } from './products/mock';
import { productFiltersRoutes } from './productFilters/mock';
import { cartRoutes } from './cart/mock';

export default function makeServer({ environment = 'test' } = {}) {
  const server = createServer({
    environment: 'development',

    models: {
      products: Model,
      cart: Model,
    },

    routes() {
      this.timing = 200;

      this.passthrough((request) => {
        return request.url.indexOf('googleapis') !== -1;
      });

      productsRoutes(this);
      productFiltersRoutes(this);
      cartRoutes(this);
    },
  });

  return server;
}
