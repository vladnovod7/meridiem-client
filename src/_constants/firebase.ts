export default {
  errors: {
    'auth/user-not-found': 'Пошта не знайдена',
    'auth/wrong-password': 'Неправильний пароль',
    'auth/email-already-in-use': 'Пошта вже використовується',
  },
};
