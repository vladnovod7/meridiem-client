import React from 'react';
import { useQuery } from 'react-query';
import { products } from '@api';
import { Row, Col } from 'antd';
import { LayoutContainer, Product } from '@ui-kit';
import { IProduct } from '@interfaces';
import * as Styled from './styles';

export default function PopularProducts() {
  const { fetchProducts, fetchProductsKey } = products;

  const { data, isLoading } = useQuery(fetchProductsKey(), fetchProducts);

  return (
    <LayoutContainer>
      <Row justify="end" gutter={[16, 5]}>
        <Col span={19}>
          <Styled.ListWrapper>
            <Row gutter={[5, 5]}>
              {
                data && data.items.map((product: IProduct) => (
                  <Col key={product.id} span={6}>
                    <Product product={product} />
                  </Col>
                ))
              }
            </Row>
          </Styled.ListWrapper>
        </Col>
      </Row>
    </LayoutContainer>
  );
}