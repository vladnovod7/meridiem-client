import { notification } from 'antd';
import firebaseConsts from '@constants/firebase';

export const showErrorFromFirebase = (e) => {
  const description = firebaseConsts.errors[e.code] || e.code;

  notification.open({
    message: 'Помилка',
    description,
    style: {
      border: '2px solid var(--error-color)',
    },
  });
};
