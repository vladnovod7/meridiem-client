import qs from 'query-string';

export const filterQueryMapper = (filters: Record<string, string[] | undefined> | undefined = {}): string => {
  const dumpedData = Object.entries(filters).reduce(
    (acc, [key, value]) => ({
      ...acc,
      ...(value?.length ? { [key]: value?.join('_') } : {}),
    }),
    {},
  );

  return qs.stringify(dumpedData);
};
