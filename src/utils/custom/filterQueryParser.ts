export const filterQueryParser = (filters: Record<string, string | string[]>): Record<string, string[] | undefined> => {
  return Object.entries(filters).reduce(
    (acc, [key, value]) => ({
      ...acc,
      [key]: value.split('_'),
    }),
    {},
  );
};
