import { union } from 'lodash';

type obj = Record<string, string[] | undefined>;

export const unionObjectInnerArrays = (obj1: obj, obj2: obj): obj => {
  const keys = union(Object.keys(obj1), Object.keys(obj2));

  return keys.reduce(
    (acc, key) => ({
      ...acc,
      [key]: union(obj1[key], obj2[key]),
    }),
    {},
  );
};
