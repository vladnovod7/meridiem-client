import { QueryFunction } from 'react-query';
import { AxiosResponse as AxiosResponseLib } from 'axios';

export interface IQueryObject {
  request(): QueryFunction | Promise<any>;
  keyCreator(): Array<any>;
}

export type AxiosResponse<T> = AxiosResponseLib<T>['data'];
