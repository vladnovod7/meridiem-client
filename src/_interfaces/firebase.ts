export interface UserPersonalData {
  name?: string;
  surname?: string;
  email?: string;
  phone?: string;
  deliveryCity?: string;
  deliveryDepartment?: string;
}
