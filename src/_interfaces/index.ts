export interface HtmlElement extends React.HTMLAttributes<HTMLDivElement> {
  title?: string;
  children: React.ReactNode;
}

export * from './products';
export * from './filters';
export * from './api';
export * from './cart';
export * from './firebase';
