export interface ICatalogType {
  id: string;
  title: string;
}

export interface ICatalogProperty {
  id: string;
  title: string;
  types: ICatalogType[];
}

export interface ICatalogOption {
  id: string;
  title: string;
  properties: ICatalogProperty[];
}
