import { ICatalogOption, ICatalogProperty, ICatalogType } from './filters';

export interface IProduct {
  id: string;
  name: string;
  price: string;
  image: string;
  properties: ICatalogProperty[];
}
export interface IFetchProductsQueries {
  productId: ICatalogOption['id'];
  propertyId: ICatalogProperty['id'];
  typeId: ICatalogType['id'];
}

export interface IFilterQueries {
  propertyId: IFetchProductsQueries['propertyId'];
  typeId: IFetchProductsQueries['typeId'];
}
