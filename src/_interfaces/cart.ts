import { IProduct } from './products';

export interface ICartProduct extends IProduct {
  count: number;
}
