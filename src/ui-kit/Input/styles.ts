import styled, { css } from 'styled-components';

interface IWrapperProps {
  error: boolean;
}

export const Wrapper = styled.div<IWrapperProps>`
  display: flex;
  flex-direction: column;

  input {
    width: 100%;

    ${({ error }) =>
      error &&
      css`
        &,
        &:hover,
        &:focus {
          border-color: var(--error-color);
          box-shadow: 0 0 0 2px rgb(255 136 155 / 49%);
        }
      `}
  }
`;

export const ErrorText = styled.span`
  margin-top: 2px;
  font-size: 12px;
  color: var(--error-color);
`;
