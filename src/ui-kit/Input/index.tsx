import React, { FC, ForwardRefRenderFunction, memo, forwardRef } from 'react';
import { Input as KitInput } from 'antd';
import { InputProps } from 'antd/lib/input';
import { useController } from 'react-hook-form';
import { Control, FieldValues } from 'react-hook-form/dist/types';
import * as Styled from './styles';

interface IInputProps extends InputProps {
  error?: boolean;
  errorText?: string;
}

interface IInputRHFProps extends InputProps {
  control: Control<FieldValues>;
  name: string;
}

export const Input: ForwardRefRenderFunction<any, IInputProps> = ({ error, errorText, ...rest }: IInputProps, ref) => {
  return (
    <Styled.Wrapper error={!!error}>
      <KitInput ref={ref} {...rest} />
      {!!errorText?.length && <Styled.ErrorText>{errorText}</Styled.ErrorText>}
    </Styled.Wrapper>
  );
};

const InputMemo = memo(forwardRef(Input));

export const InputRHF: FC<IInputRHFProps> = ({ name, control, ...rest }: IInputRHFProps) => {
  const {
    field,
    fieldState: { error, ...inputState },
  } = useController({
    name,
    control,
    defaultValue: '',
  });

  return <InputMemo error={!!error?.message} errorText={error?.message} {...field} {...inputState} {...rest} />;
};

export { Wrapper as StyledInputWrapper } from './styles';
