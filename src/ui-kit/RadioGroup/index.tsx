import React, { FC } from 'react';
import { Radio } from 'antd';
import { RadioGroupProps } from 'antd/lib/radio';
import { Control, FieldValues } from 'react-hook-form/dist/types';
import { useController } from 'react-hook-form';

const RadioGroup: FC<RadioGroupProps> = (props: RadioGroupProps) => {
  return <Radio.Group {...props} />;
};

interface IRadioGroupRHFProps extends RadioGroupProps {
  control: Control<FieldValues>;
  name: string;
  defaultValue?: string;
}

const RadioGroupRHF: FC<IRadioGroupRHFProps> = ({
  name,
  control,
  defaultValue,
  ...additionalProps
}: IRadioGroupRHFProps) => {
  const { field, fieldState } = useController({
    name,
    control,
    defaultValue,
  });

  return <RadioGroup {...field} {...fieldState} {...additionalProps} />;
};

export { RadioGroup, RadioGroupRHF };
