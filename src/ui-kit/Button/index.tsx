import React from 'react';
import { ButtonProps } from 'antd/lib/button';
import * as Styled from './styles';

export default function Button(props: ButtonProps) {
  return <Styled.Button {...props} />;
}