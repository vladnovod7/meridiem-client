import styled from 'styled-components';
import { Button as KitButton } from 'antd';

export const Button = styled(KitButton)`
&.ant-btn{
  border-radius: 8px;
  font-weight: 700;
  color: black;
}
`;