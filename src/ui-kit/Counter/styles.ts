import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  color: var(--primary);
  padding: 3px 5px;
  border-radius: 3px;
  background: var(--lightGray);

  svg {
    height: 20px;
    width: 20px;
    cursor: pointer;
  }

  span {
    &:first-child {
      ${({ count }) =>
        count === 1 &&
        css`
          opacity: 0.3;
          pointer-events: none;
        `}
    }
  }
`;

export const Value = styled.div`
  margin: 0 5px;
  background: white;
  border-radius: 3px;
  height: 100%;
  padding: 0 7px;
`;
