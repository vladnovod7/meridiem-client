import React, { FC } from 'react';
import { Input } from 'antd';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';
import * as Styled from './styles';

interface ICouterProps {
  value: number | string;
  onDecrement(): void;
  onIncrement(): void;
}

const Counter: FC<ICouterProps> = ({ value, onDecrement, onIncrement }: ICouterProps) => {
  return (
    <Styled.Wrapper count={value}>
      <MinusOutlined onClick={onDecrement} />
      <Styled.Value>{value}</Styled.Value>
      <PlusOutlined onClick={onIncrement} />
    </Styled.Wrapper>
  );
};

export default Counter;
