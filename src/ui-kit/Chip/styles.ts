import styled from 'styled-components';

export const Wrapper = styled.div`
  background: rgb(223, 155, 109);
  background: linear-gradient(90deg, rgba(223, 155, 109, 1) 0%, rgba(255, 173, 0, 1) 100%);
  border: 1px solid #ffb56a;
  padding: 4px 8px;
  border-radius: 20px;
  font-size: 12px;
  width: fit-content;
`;

export const Title = styled.span`
  color: white;

  svg {
    margin-left: 8px;
  }
`;
