import React, { FC, ReactNode, useCallback } from 'react';
import { CloseOutlined } from '@ant-design/icons';
import * as Styled from './styles';

interface IChipProps {
  payload: any[];
  onClose(payload: any[]): void;
  children: ReactNode;
}

const Chip: FC<IChipProps> = ({ payload, children, onClose }: IChipProps) => {
  const handleChipCloseClick = useCallback(() => onClose(payload), [payload, onClose]);

  return (
    <Styled.Wrapper>
      <Styled.Title>
        {children}
        <CloseOutlined onClick={handleChipCloseClick} />
      </Styled.Title>
    </Styled.Wrapper>
  );
};

export default Chip;
