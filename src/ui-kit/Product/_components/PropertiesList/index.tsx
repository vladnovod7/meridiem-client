import React, { FC } from 'react';
import { ICatalogProperty } from '@interfaces';
import * as Styled from './styles';

interface IPropertiesListProps {
  properties: ICatalogProperty[];
}

const PropertiesList: FC<IPropertiesListProps> = ({ properties }: IPropertiesListProps) => {
  return (
    <Styled.Wrapper>
      <Styled.Title>Характеристики</Styled.Title>
      {properties.map(({ id, title, types }) => (
        <Styled.PropertyWrapper key={id}>
          <Styled.PropertyName>{title}:</Styled.PropertyName>
          <Styled.PropertyValue>{types[0].title}</Styled.PropertyValue>
        </Styled.PropertyWrapper>
      ))}
    </Styled.Wrapper>
  );
};

export default PropertiesList;
