import styled from 'styled-components';

export const Title = styled.span`
  margin-bottom: 6px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const PropertyWrapper = styled.div`
  font-size: 12px;
  background: var(--lightGray);
  padding: 2px 7px;
`;

export const PropertyName = styled.span``;

export const PropertyValue = styled.span`
  margin-left: 4px;
  color: var(--primary);
`;
