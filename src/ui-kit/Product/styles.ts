import styled, { css } from 'styled-components';
import { Wrapper as PropertyListWrapper } from './_components/PropertiesList/styles';

export const OverfowWrapper = styled.div`
  position: relative;
  max-height: 310px;

  &:hover {
    z-index: 2;
  }
`;

export const Wrapper = styled.div`
  padding: 15px;
  box-shadow: 0px 0px 4px -1px rgba(34, 60, 80, 0.2);
  display: flex;
  flex-direction: column;
  border: 2px solid white;
  transition: border-color 0.2s;
  max-height: 310px;
  overflow: hidden;
  background: white;

  &:hover {
    border-color: var(--secondary);
    max-height: unset;
  }

  .ant-divider {
    margin: 12px 0;
  }

  ${PropertyListWrapper} {
    margin-top: 10px;
  }
`;

export const Image = styled.img`
  height: 192px;
  min-height: 192px;
  object-fit: contain;
  width: auto;
  margin-bottom: 6px;
`;

export const Name = styled.h4`
  color: var(--primary);
  margin-bottom: 10px;
  min-height: 44px;
  cursor: pointer;
`;

export const Price = styled.p`
  align-self: flex-start;
  font-size: 21px;
  font-weight: 500;

  &::after {
    font-size: 10px;
    margin-left: 4px;
    content: 'грн';
  }
`;

export const RemoveFromCart = styled.span`
  margin-left: 10px;
  font-size: 12px;
  color: gray;
  text-decoration: underline;
  cursor: pointer;
`;

export const BuyButtonWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;

  ${RemoveFromCart} {
    flex-shrink: 1;
  }

  > button {
    width: 100%;
    transition: unset;

    ${({ isProductAlreadyInCart }) =>
      isProductAlreadyInCart &&
      css`
        width: 40px;
      `};
  }
`;
