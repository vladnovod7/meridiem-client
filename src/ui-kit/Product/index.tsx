import React, { FC, useCallback, useMemo } from 'react';
import { ShoppingCartOutlined, LoadingOutlined } from '@ant-design/icons';
import { useCart } from '@hooks';
import { Divider, Button } from 'antd';
import { IProduct } from '@interfaces';
import { PropertiesList } from './_components';
import * as Styled from './styles';
export interface IProductsProps {
  product: IProduct;
  type?: 'vertical' | 'horizontal';
  onTitleClick?(id: IProduct['id']): void;
}

const Product: FC<IProductsProps> = ({
  product: { id, name, image, price, properties },
  onTitleClick,
}: IProductsProps) => {
  const { data: cartData, add, remove, isFetching, addLoading, removeLoading } = useCart();

  const handleClickBuy = useCallback(() => {
    add(id);
  }, [add, id]);

  const handleRemoveClick = useCallback(() => {
    remove(id);
  }, [id, remove]);

  const handleClickOnProductTitle = useCallback(() => {
    if (onTitleClick) onTitleClick(id);
  }, [id, onTitleClick]);

  const isProductAlreadyInCart = useMemo(
    () => !!cartData?.find(({ id: productId }) => productId === id),
    [cartData, id],
  );

  const isLoading = isFetching || addLoading || removeLoading;

  return (
    <Styled.OverfowWrapper>
      <Styled.Wrapper>
        <Styled.Image src={image} />
        <Styled.Name onClick={handleClickOnProductTitle}>{name}</Styled.Name>
        <Styled.Price>{price}</Styled.Price>
        <Divider />
        <Styled.BuyButtonWrapper isProductAlreadyInCart={isProductAlreadyInCart}>
          <Button
            disabled={isProductAlreadyInCart}
            type="primary"
            icon={isLoading ? <LoadingOutlined /> : <ShoppingCartOutlined />}
            onClick={handleClickBuy}
          >
            {isProductAlreadyInCart ? '' : 'Купити'}
          </Button>
          {isProductAlreadyInCart && (
            <Styled.RemoveFromCart onClick={handleRemoveClick}>Прибрати з кошика</Styled.RemoveFromCart>
          )}
        </Styled.BuyButtonWrapper>
        <PropertiesList properties={properties} />
      </Styled.Wrapper>
    </Styled.OverfowWrapper>
  );
};

Product.defaultProps = {
  type: 'vertical',
};

export default Product;
