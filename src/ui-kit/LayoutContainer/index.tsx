import React from 'react';
import { Row, Col } from 'antd';

export interface LayoutContainer {
  children: React.ReactNode,
}

export default function LayoutContainer({ children }: LayoutContainer) {
  return (
    <Row>
      <Col span={2} />
      <Col span={20}>{children}</Col>
      <Col span={2} />
    </Row>
  )
}