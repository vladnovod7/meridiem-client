import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 1200px;
  padding: 0 calc((100vw - 1200px) / 2);
`;