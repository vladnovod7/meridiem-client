import React from 'react';
import { Card as KitCard } from 'antd';
import { CardProps } from 'antd/lib/card';
import styled from 'styled-components';

function Card(props: CardProps) {
  return <KitCard {...props} />
}

export default styled(Card)``;